import json
import os
import io
import sys
import pytesseract
import cv2

from PIL import Image
from werkzeug.utils import secure_filename
from flask import Flask, request, redirect, url_for, flash

BASE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))
BASE_FOLDER = os.path.join(BASE_DIRECTORY)
UPLOAD_FOLDER = os.path.join(BASE_DIRECTORY, 'static')
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
MAX_FILE_SIZE = 8 * 1024 * 1024
MATCH_THRESHOLD = 0.90

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = MAX_FILE_SIZE
pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract'

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_image():
    if request.method == 'POST':
        if 'file' not in request.files:
            print('No file')
            return redirect(request.url)

        file = request.files['file']

        if file.filename == '':
            print('No selected file')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            route = os.path.join(BASE_DIRECTORY, app.config['UPLOAD_FOLDER'], filename)
            file.save(route)
            return process_image(route)

    return '''
	<!DOCTYPE html>
	<title>Oscar</title>
	<h1>Upload image</h1>
	<form method=post enctype=multipart/form-data>
	  <p><input type=file name=file>
		 <input type=submit value=Upload>
	</form>
	'''

def process_image(filename):
    ent = cv2.imread(filename)
    ref = cv2.cvtColor(ent, cv2.COLOR_BGR2GRAY)
    ref = cv2.threshold(ref, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    ref = cv2.medianBlur(ref, 3)

    file = "{}.png".format(os.getpid())
    cv2.imwrite(file, ref)

    img = Image.open(file)
    resa = pytesseract.image_to_string(img, lang="ocra")
    resb = pytesseract.image_to_string(img, lang="ocrb")
    eng = pytesseract.image_to_string(img, lang="eng")
    spa = pytesseract.image_to_string(img, lang="spa")
    return json.dumps({ 'ocra': resa, 'ocrb': resb, 'eng': eng, 'spa': spa })


if __name__ == '__main__':
	app.run(debug=True)